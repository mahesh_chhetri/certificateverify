// Car details toggle
$("#list-container .toggle-details").click(function (e) {
    e.preventDefault();
    // debugger;
    $(this).find('i').toggleClass('fa-chevron-up fa-chevron-down');
    var parentContainer = $(this).closest('.list');
    parentContainer.toggleClass('details-shown');
    parentContainer.next().toggleClass('d-none');
});

// Car inner details tab

$(".detail-innerTabs li").click(function (e) {
    e.preventDefault();
    // $(".detail-innerTabs li").removeClass('active');
    var listElem = $(this).closest('.detail-innerTabs');
    $(listElem).find('li').removeClass('active');
    $(this).addClass('active');
    var index = $(this).index();
    var allTabsContent = $(listElem.next()).children();
    allTabsContent.removeClass('active');
    $(allTabsContent[index]).addClass('active');
})