$(function () {
    $(".navbar-nav  li:has(ul)").find("a").click(function () {
        var parent = $(this).parent();
        parent.siblings().find("ul.show-subnav").removeClass("show-subnav");
        parent.find("ul:first").toggleClass("show-subnav");
    });
});